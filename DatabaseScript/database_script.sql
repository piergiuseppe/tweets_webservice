DROP DATABASE IF EXISTS tweets_db;
CREATE DATABASE tweets_db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE tweets_db;

DROP TABLE IF EXISTS user;
CREATE TABLE user (
id int(10) NOT NULL auto_increment,
name varchar(255) NOT NULL,
twitter_nickname varchar(255) NOT NULL,
joined_date TIMESTAMP NOT NULL, 
PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS tweet;
CREATE TABLE tweet (
tweet_id int(10) NOT NULL auto_increment,
author_id int(10) NOT NULL,
message varchar(255) NOT NULL,
date TIMESTAMP NOT NULL,
PRIMARY KEY(tweet_id),
FOREIGN KEY (author_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

INSERT INTO user(name,twitter_nickname, joined_date) VALUES
('Alex Vasile','@alexv', CURRENT_DATE()),
('John Skeet','@johns', CURRENT_DATE()),
('Aija Baumane','@aijab', CURRENT_DATE()),
('Pawan Reddy','@pawanr', CURRENT_DATE()),
('John Doe','@johnd', CURRENT_DATE());

INSERT INTO tweet(author_id,message,date) VALUES
(1, 'My first tweet!', CURRENT_DATE()),
(1, 'My second tweet!', CURRENT_DATE()),
(2, 'A cool tweet!', CURRENT_DATE()),
(2, 'And another cool tweet!', CURRENT_DATE()),
(3, 'This is awesome!', CURRENT_DATE()),
(3, 'Indeed it is!', CURRENT_DATE()),
(4, 'Amazing tweet skills!', CURRENT_DATE()),
(4, 'Super impressive!', CURRENT_DATE()),
(5, 'Super cool!', CURRENT_DATE()),
(5, 'Tweeting is cool!', CURRENT_DATE());