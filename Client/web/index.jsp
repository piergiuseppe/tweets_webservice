<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
  <title>Tweets</title>
  <meta content="Tweets Project ISEP" name="description">
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
  <link rel="icon" href="./images/favicon.png">
  <link rel="shortcut icon" href="./images/favicon.png"/>

  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./bower_components/960-grid-system/code/css/960_12_col.css">
  <link rel="stylesheet" href="./bower_components/simply_toast/simply-toast.min.css">

  <script src="./bower_components/jquery/dist/jquery.min.js"></script>
  <script src="./bower_components/superfish/dist/js/superfish.min.js"></script>
  <script src="./bower_components/jquery.easing/js/jquery.easing.min.js"></script>
  <script src="./bower_components/classie/classie.js"></script>
  <script src="./bower_components/simply_toast/simply-toast.min.js"></script>




</head>
<body class="">
<!--==============================header=================================-->
<header>
  <div class="container_12">
    <div class="grid_12"><a><img title="tweets Italy Vacation Rentals" src="./images/logo.png"
                                 alt="tweets" id="logo_image">
    </a>

      <div class="menu_block">
        <nav class="">
          <ul class="sf-menu">
            <li><a id="home">HOME</a></li>
            <li><a id="allusers">ALL USERS</a></li>
            <li><a id="alltweets">ALL TWEETS</a></li>
            <li><a id="update">UPDATE</a></li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="hidden_logos">
      <div class="isep_logo"><img title="ISEP" src="./images/isep_logo.png" alt="ISEP">

      </div>
    </div>
  </div>
</header>


<div class="content">
  <div class="container_12">
    <div class="grid_3" id="users_list_container">
      <h3 class="head1" id="user_name">USERS</h3>
      <ul class="list" data-columns="3" id="users_list">

      </ul>
    </div>


    <div class="grid_9" id="user_tweets">
      <section id="tweets_timeline" class="timeline_container">

      </section>
    </div>


    <div class="grid_9" id="tweets">
      <h3><span>ALL TWEETS</span></h3>
      <div id="wrapper_pin">
        <div id="tweets_wall" class="col_number_3">

        </div>
      </div>


    </div>


  </div>
</div>

<!--==============================footer=================================-->

<footer>
  <div class="container_12" style="text-align:center">
    <div class="grid_12">
      <div class="tweets_footer"><strong> Twitter - Servlet - MySQL - JSP - jQuery - Ajax - Json - 2015 </strong>
        <a href="mailto:info@tweets.com">piergiuseppe.mallozzi@gmail.com</a></div>
    </div>
  </div>
</footer>



<script>
  function init() {
    window.addEventListener('scroll', function (e) {
      var distanceY = window.pageYOffset || document.documentElement.scrollTop,
              shrinkOn = 50,
              header = document.querySelector("header");
      if (distanceY > shrinkOn) {
        classie.add(header, "smaller");
      } else {
        if (classie.has(header, "smaller")) {
          classie.remove(header, "smaller");
        }
      }
    });
  }
  window.onload = init();

  jQuery(document).ready(function () {
    jQuery('ul.sf-menu').superfish();


    $('#user_tweets').hide();

    // Get All Users
    $.get('/TweetsWebServiceClient/homepage?action=allusers', function(responseText) {
      $('#users_list').html(responseText);
    });

    // Get All Tweets
    $.get("/TweetsWebServiceClient/homepage?action=alltweets", function(responseText) {
      $('#tweets_wall').html(responseText);
    });
//    $('div.tweet_nickname').hide();


  });


  $('#home').click(function() {
    $('#user_tweets').hide();
    $('#user_name').text("USERS");

    $('#tweets').removeClass().addClass('grid_9').show(300,'swing');
    $('#users_list_container').removeClass().addClass('grid_3').show(300,'swing');
    $('#tweets_wall').removeClass().addClass('col_num_3');
    $('#tweets h3 span').text("ALL TWEETS");
    $.get('/TweetsWebServiceClient/homepage?action=allusers', function(responseText) {
      $('#users_list').html(responseText);
    });
    $.get("/TweetsWebServiceClient/homepage?action=alltweets", function(responseText) {
      $('#tweets_wall').html(responseText);
    });
  });

  $('#allusers').click(function() {
    $('#user_tweets').hide();
    $('#tweets').hide();
    $('#user_name').text("USERS");

    $('#users_list_container').removeClass().addClass('grid_12').show();
    $.get('/TweetsWebServiceClient/homepage?action=allusers', function(responseText) {
      $('#users_list').html(responseText);
    });
  });

  $('#alltweets').click(function() {
    $('#users_list_container').hide();
    $('#user_tweets').hide();
    $('#user_name').text("USERS");

    $('#tweets').removeClass().addClass('grid_12').show();
    $('#tweets_wall').removeClass().addClass('col_num_5');
    $('#tweets h3 span').text("ALL TWEETS");
    $.get("/TweetsWebServiceClient/homepage?action=alltweets", function(responseText) {
      $('#tweets_wall').html(responseText);
    });
  });

  $('#update').click(function() {
    $.get("/TweetsWebServiceClient/homepage?action=update");
    $.simplyToast('Data updated successfully!', 'success');

    $.get("/TweetsWebServiceClient/homepage?action=alltweets", function(responseText) {
      $('#tweets_wall').html(responseText);
    });
    $.get('/TweetsWebServiceClient/homepage?action=allusers', function(responseText) {
      $('#users_list').html(responseText);
    });
  });

  // Dinamically generated so different method used...
  $('body').on('click', 'a.tweet_user', function() {
    $('#tweets').hide();
    var user_id = this.id;
    var text = $(this).text();
    $('#user_name').text("USER - "+user_id)
    $('#user_tweets h3 span').text("TWEETS FROM " + text);
    $('#users_list_container').removeClass().addClass('grid_3').show(300,'swing');
    $.get("/TweetsWebServiceClient/homepage?action=tweets&user_id=" + user_id, function(responseText) {
      $('#tweets_timeline').html(responseText);
    });
    $('#user_tweets').show();

  });


</script>


</body>
</html>