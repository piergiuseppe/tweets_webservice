/**
 * Tweet.java
 * @author  Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package model;

import java.sql.Date;

public class Tweet {

    private Integer id;
    private Integer author_id;
    private String message;
    private Date date;

    public Tweet() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(Integer author_id) {
        this.author_id = author_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Tweet [id=" + id + ", author_id=" + author_id + ", message="
                + message + ", date=" + date + "]";
    }
}
