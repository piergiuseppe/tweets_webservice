/**
 * User.java
 * @author  Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package model;

import java.sql.Date;

public class User {

	private Integer id;
	private String name;
	private String twitter_nickname;
	private Date joined_date;

	public User() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTwitter_nickname() {
		return twitter_nickname;
	}

	public void setTwitter_nickname(String twitter_nickname) {
		this.twitter_nickname = twitter_nickname;
	}

	public Date getJoined_date() {
		return joined_date;
	}

	public void setJoined_date(Date joined_date) {
		this.joined_date = joined_date;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", twitter_nickname="
				+ twitter_nickname + ", joined_date=" + joined_date + "]";
	}

}
