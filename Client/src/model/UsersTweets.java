/**
 * UsersTweets.java
 * @author  Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package model;

import java.util.List;

public class UsersTweets {

    private User user;
    private List<Tweet> tweetList;

    public UsersTweets() {

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Tweet> getTweetList() {
        return tweetList;
    }

    public void setTweetList(List<Tweet> tweetList) {
        this.tweetList = tweetList;
    }
}
