/**
 * ClientServlet.java
 *
 * @author Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package testing;

import model.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import rest.RESTClient;

/**
 * Servlet implementation class TestClient
 */
@WebServlet("/TestClient")
public class ClientServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public ClientServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        String user_nickname = request.getParameter("user_id");

        System.out.println("------ DO GET -> action=" + action + " & userid=" + user_nickname);

        if (action == null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
            dispatcher.forward(request, response);
        } else {
            if (action.equals("allusers")) {

                response.setContentType("text/html");  // Set content type of the response so that jQuery knows what it can expect.
                response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
                PrintWriter pw = response.getWriter();

                List<User> listOfUsers = new ArrayList<User>();
                listOfUsers = RESTClient.getUsers();

                for (User user : listOfUsers) {
                    pw.println("<li><a class = 'tweet_user' id = '" + user.getTwitter_nickname() + "' >");
                    pw.print(user.getName());
                    pw.print(" (");
                    pw.print(user.getTwitter_nickname());
                    pw.println(" )");
                    pw.println("</a></li>");
                }
            }
            if (action.equals("alltweets")) {
                response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
                response.setCharacterEncoding("UTF-8"); // You want world domination, huh?

                PrintWriter pw = response.getWriter();

                List<UsersTweets> lisOfUsersTweets = new ArrayList<UsersTweets>();
                lisOfUsersTweets = RESTClient.getAllTweets();

                for (UsersTweets user_tweets : lisOfUsersTweets) {

                    List<Tweet> listofTweets = new ArrayList<Tweet>();
                    listofTweets = user_tweets.getTweetList();
                    pw.println("<div class='tweet_nickname'><h2>" + user_tweets.getUser().getTwitter_nickname() + "</h2></div>");
                    for (Tweet tweet : listofTweets) {
                        pw.println("<div class='pin'><p>");
                        pw.print(tweet.getMessage());
                        System.out.println(tweet.getMessage());
                        pw.println("</p></div>");
                    }
                }
            }
            if (action.equals("tweets") && user_nickname != null) {
                response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
                response.setCharacterEncoding("UTF-8"); // You want world domination, huh?

                PrintWriter pw = response.getWriter();

                List<Tweet> listofTweets = new ArrayList<Tweet>();
                listofTweets = RESTClient.getTweets(user_nickname);

                for (Tweet tweet : listofTweets) {
                    pw.println("<div class=\"tweet_timeline_block\"><div class=\"tweet_timeline_content\"><p>");
                    pw.print(tweet.getMessage());
                    pw.print("</p><span class=\"tweet_date\">");
                    pw.print(tweet.getDate());
                    pw.print("</span></div></div>");
                }
            }
            if (action.equals("update")) {

                RESTClient.updateData();

                System.out.println("");
                System.out.println("");
                System.out.println("DATA UPDATED");
                System.out.println("");
                System.out.println("");
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }
}
