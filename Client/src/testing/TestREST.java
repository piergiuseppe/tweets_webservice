/**
 * TestREST.java
 *
 * @author Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package testing;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import rest.RESTClient;
import model.Tweet;
import model.User;
import model.UsersTweets;

public class TestREST {

    static Logger log = Logger.getLogger(TestREST.class);

    public static void updateDataTest() {

        RESTClient.updateData();
    }

    public static void getUsersTest() {

        List<User> userList = new ArrayList<User>();
        userList = RESTClient.getUsers();

        for (User user : userList) {
            log.info(user.getName() + "  " + user.getTwitter_nickname());
        }
    }

    public static void getTweetsTest(String nickname) {

        List<Tweet> tweetList = new ArrayList<Tweet>();
        tweetList = RESTClient.getTweets(nickname);

        for (Tweet tweet : tweetList) {
            log.info(tweet.getAuthor_id() + "  " + tweet.getMessage());
        }
    }

    public static void getAllTweetsTest() {

        List<UsersTweets> usersTweetsList = new ArrayList<UsersTweets>();
        usersTweetsList = RESTClient.getAllTweets();

        for (UsersTweets usersTweets : usersTweetsList) {
            log.info(usersTweets.getUser().getTwitter_nickname());

            List<Tweet> tweetList = new ArrayList<Tweet>();
            tweetList = usersTweets.getTweetList();
            for (Tweet tweet : tweetList) {
                log.info(tweet.getMessage());
            }
        }
    }
}
