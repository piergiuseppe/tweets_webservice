/**
 * RESTClient.java
 * @author  Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import model.Tweet;
import model.User;
import model.UsersTweets;

public class RESTClient {

    public static void updateData() {

        Client client = ClientBuilder.newClient();

        client.target(getBaseURI()).path("updateData").request().get();

        // Needs to be changed to PUT/POST
    }

    public static List<Tweet> getTweets(String nickname) {

        Client client = ClientBuilder.newClient();

        List<Tweet> tweetsList = new ArrayList<Tweet>();
        tweetsList = client.target(getBaseURI()).path("getTweets")
                .queryParam("nickname", nickname)
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<Tweet>>() {
                });

        return tweetsList;
    }

    public static List<UsersTweets> getAllTweets() {

        List<UsersTweets> userTweetsList = new ArrayList<UsersTweets>();
        List<User> userList = new ArrayList<User>();
        List<Tweet> tweetList = new ArrayList<Tweet>();
        userList = getUsers();

        for (User user : userList) {
            tweetList = getTweets(user.getTwitter_nickname());
            UsersTweets usersTweets = new UsersTweets();
            usersTweets.setUser(user);
            usersTweets.setTweetList(tweetList);
            userTweetsList.add(usersTweets);
        }

        return userTweetsList;
    }

    public static List<User> getUsers() {

        Client client = ClientBuilder.newClient();

        List<User> usersList = new ArrayList<User>();
        usersList = client.target(getBaseURI()).path("getUsers")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<User>>() {
                });

        return usersList;
    }

    public static String getBaseURI() {

        return "http://localhost:8080/TweetsWebService/rest/resource";
    }
}
