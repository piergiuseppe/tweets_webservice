/**
 * RESTResource.java
 *
 * @author Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import model.Tweet;
import model.User;
import db.DBHelper;

@Path("/resource")
public class RESTResource {

    static Logger log = Logger.getLogger(RESTResource.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/updateData")
    public void updateData() {

        log.info("'updateData()' called on the server.");

        DBHelper.updateData();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getUsers")
    public List<User> getUsers() {

        log.info("'getUsers()' called on the server.");

        List<User> listOfUsers = new ArrayList<User>();
        listOfUsers = DBHelper.getUsers();
        return listOfUsers;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getTweets")
    public List<Tweet> getTweets(@QueryParam("nickname") String nickname) {

        log.info("'getTweets(String nickname)' called on the server.");

        List<Tweet> listOfTweets = new ArrayList<Tweet>();

        if (nickname != null && !nickname.isEmpty()) {

            listOfTweets = DBHelper
                    .getTweets(getTwitterIdForNickname(nickname));

            if (listOfTweets.size() == 0) {
                handleErrorInvalidParameter(nickname);
            }
        } else {
            handleErrorParameterMissing();
        }

        return listOfTweets;
    }

    private static void handleErrorInvalidParameter(String nickname) {
        throw new WebApplicationException(
                Response.status(Status.BAD_REQUEST)
                        .entity("The nickname '"
                                + nickname
                                + "' does not exist in the database. Please retry your request with a different value.")
                        .build());
    }

    private static void handleErrorParameterMissing() {
        throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
                .entity("Please provide the 'nickname' parameter. ").build());
    }

    private static Integer getTwitterIdForNickname(String nickname) {

        List<User> userList = new ArrayList<User>();
        userList = DBHelper.getUsers();

        for (User user : userList) {
            if (user.getTwitter_nickname().equals(nickname)) {
                return user.getId();
            }
        }

        return 0;
    }
}
