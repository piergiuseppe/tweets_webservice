/**
 * DBHelper.java
 *
 * @author Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Tweet;
import model.User;

import org.apache.log4j.Logger;

public class DBHelper {

    private static String DBURL = "jdbc:mysql://localhost:3306/tweets_db"
            + "?useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8";
    private static String DBLOGIN = "root";
    private static String DBPASSWORD = "";
    static Logger log = Logger.getLogger(DBHelper.class);

    public static void updateData() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rset = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            log.error("MySQL driver not found.");
            // e.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(DBURL, DBLOGIN, DBPASSWORD);
            stmt = conn.createStatement();
            rset = stmt.executeQuery(SQLQueries.COUNT_USERS);
            rset.next();
            // Updating only if the 'users' table is empty
            if (rset.getInt(1) == 0) {
                stmt.executeUpdate(SQLQueries.INSERT_USERS);
                // The next query works only if all the users have been deleted
                // once. (to
                // reset the auto-increment)
                stmt.executeUpdate(SQLQueries.INSERT_TWEETS);
                log.info("Database updated with 5 users and 10 tweets.");
            } else {
                log.info("Database was not updated because it's not empty.");
            }
        } catch (SQLException e) {
            log.error("Connection to MySQL failed or a SQLException occured.");
            // e.printStackTrace();
        } finally {
            try {
                if (rset != null)
                    rset.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static List<User> getUsers() {
        List<User> userList = new ArrayList<User>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rset = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            log.error("MySQL driver not found.");
            // e.printStackTrace();
            return null;
        }
        try {
            conn = DriverManager.getConnection(DBURL, DBLOGIN, DBPASSWORD);
            stmt = conn.createStatement();
            rset = stmt.executeQuery(SQLQueries.GET_USERS);
            while (rset.next()) {

                User user = new User();
                user.setId(rset.getInt("id"));
                user.setName(rset.getString("name"));
                user.setTwitter_nickname(rset.getString("twitter_nickname"));
                user.setJoined_date(rset.getDate("joined_date"));

                userList.add(user);
                log.info(user.toString());
            }
        } catch (SQLException e) {
            log.error("Connection to MySQL failed or a SQLException occured.");
            // e.printStackTrace();
        } finally {
            try {
                if (rset != null)
                    rset.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        log.info("Found a total of " + userList.size() + " user(s).");

        return userList;
    }

    public static List<Tweet> getTweets(long userid) {
        List<Tweet> tweetsList = new ArrayList<Tweet>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rset = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            log.error("MySQL driver not found.");
            // e.printStackTrace();
            return null;
        }
        try {
            conn = DriverManager.getConnection(DBURL, DBLOGIN, DBPASSWORD);
            stmt = conn.createStatement();
            rset = stmt.executeQuery(SQLQueries.GET_TWEET + userid);
            while (rset.next()) {

                Tweet tweet = new Tweet();
                tweet.setId(rset.getInt("tweet_id"));
                tweet.setAuthor_id(rset.getInt("author_id"));
                tweet.setMessage(rset.getString("message"));
                tweet.setDate(rset.getDate("date"));

                tweetsList.add(tweet);
                log.info(tweet.toString());
            }
        } catch (SQLException e) {
            log.error("Connection to MySQL failed or a SQLException occured.");
            // e.printStackTrace();
        } finally {
            try {
                if (rset != null)
                    rset.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (stmt != null)
                    stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        log.info("Listed tweets for author_id: " + userid);
        return tweetsList;
    }
}
