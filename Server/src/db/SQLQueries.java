/**
 * SQLQueries.java
 *
 * @author Piergiuseppe Mallozzi (piergiuseppe.mallozzi@gmail.com)
 * @date December 2014
 * @version 1.0
 */

package db;

public class SQLQueries {

    public final static String GET_USERS = "SELECT * FROM user";
    public final static String COUNT_USERS = "SELECT COUNT(*) FROM user";
    public final static String GET_TWEET = "SELECT * FROM `tweet`WHERE author_id = ";
    public final static String INSERT_USERS = "INSERT INTO user(name,twitter_nickname, joined_date) VALUES"
            + "('Alex Vasile','@alexv', CURRENT_DATE()),"
            + "('John Skeet','@johns', CURRENT_DATE()),"
            + "('Aija Baumane','@aijab', CURRENT_DATE()),"
            + "('Pawan Reddy','@pawanr', CURRENT_DATE()),"
            + "('John Doe','@johnd', CURRENT_DATE());";
    public final static String INSERT_TWEETS = "INSERT INTO tweet(author_id,message,date) VALUES"
            + "(6, 'My first tweet!', CURRENT_DATE()),"
            + "(6, 'My second tweet!', CURRENT_DATE()),"
            + "(7, 'A cool tweet!', CURRENT_DATE()),"
            + "(7, 'And another cool tweet!', CURRENT_DATE()),"
            + "(8, 'This is awesome!', CURRENT_DATE()),"
            + "(8, 'Indeed it is!', CURRENT_DATE()),"
            + "(9, 'Amazing tweet skills!', CURRENT_DATE()),"
            + "(9, 'Super impressive!', CURRENT_DATE()),"
            + "(10, 'Super cool!', CURRENT_DATE()),"
            + "(10, 'Tweeting is cool!', CURRENT_DATE());";
}
